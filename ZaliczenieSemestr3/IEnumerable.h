#pragma once
#include <functional>

template <typename T>
class IEnumerable {
public:
	virtual ~IEnumerable() = default;
	typedef std::function<void(T)> EnumerableCallback;
	virtual void foreach(EnumerableCallback func) = 0;
};