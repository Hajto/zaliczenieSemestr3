#pragma once
#include "IStack.h"
#include <vector>

template <typename T>
class VectorStack : public IStack<T>
{
public:
	VectorStack();
	~VectorStack();

	//IStack members override
	void push(T elem) override;
	T pop() override;
	T head() override;
	void reverse() override;
	int length() override;
	bool isEmpty() override;

	//IEnumerable members
	void foreach(EnumerableCallback) override;

private:
	std::vector<T> container_vector;
	int position = 0;
};
