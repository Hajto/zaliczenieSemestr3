#include "stdafx.h"
#include "VectorStack.h"

template <typename T>
VectorStack<T>::VectorStack()
{
}

template <typename T>
VectorStack<T>::~VectorStack()
{
}

template <typename T>
void VectorStack<T>::push(T elem)
{
	container_vector.push_back(elem);
	position++;
}

template <typename T>
T VectorStack<T>::pop()
{
	if(!isEmpty())
	{
		T temp = container_vector[position-1];
		container_vector.pop_back();
		position--;
		return temp;
	}
	throw StackAlreadyEmptyException();
}

template <typename T>
T VectorStack<T>::head()
{
	return container_vector.back();
}

template <typename T>
void VectorStack<T>::reverse()
{
	std::reverse(container_vector.begin(), container_vector.end());
}

template <typename T>
int VectorStack<T>::length()
{
	return container_vector.size();
}

template <typename T>
bool VectorStack<T>::isEmpty()
{
	return container_vector.size() == 0;
}

/*
 * IEnumerable members
 */

template <typename T>
void VectorStack<T>::foreach(EnumerableCallback function) {
	for (std::vector<T>::iterator it = container_vector.begin(); it != container_vector.end(); ++it) {
		function(*it);
	}
}
