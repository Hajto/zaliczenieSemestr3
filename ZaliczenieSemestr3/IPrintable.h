#pragma once
#include <sstream>
#include "string"

class IPrintable {
public:
	virtual std::string print_to_string() = 0;

	template<typename T> 
	static std::string print_to_string(const T& x) {
		std::ostringstream os;
		os << x;
		return os.str();
	}
	static std::string print_to_string(IPrintable* printable) {
		return printable->print_to_string();
	}
};