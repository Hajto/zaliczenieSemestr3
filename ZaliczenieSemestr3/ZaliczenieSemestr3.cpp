#include "stdafx.h"
#include "MemoryManager.h"
#include "LinkedListStack.h"
#include "iostream"
#include "VectorStack.h"
#include <fstream>

template <typename T>
void printAnyStack(IStack<T>* stack);

int main()
{
	//Startup
	MemoryManager manager;
	LinkedListStack<int> stack;
	VectorStack<int> vs;
	

	// Try catching an exception
	try{
		stack.pop();
	} catch(StackAlreadyEmptyException e){
		std::cout << e.what() << std::endl;
	}

	//Inserted some junk data
	for (int i = 0; i < 10; i++) {
		stack.push(i * i);
		vs.push(i*i);
	}
	std::cout << "Linked stack" << std::endl;
	std::cout << &stack << std::endl;
	std::cout << "Vector stack" << std::endl;
	printAnyStack<int>(&vs);

	stack + &vs;

	std::cout << "After adding" << std::endl;
	printAnyStack(&stack);

	std::ofstream myfile;
	myfile.open("example.txt");
	myfile << "Po��czony stos!:" << std::endl;
	myfile << &stack;
	myfile.close();

    return 0;
}


/*
 * Polymorphic function that will accept any object that is covriable to IStack
 */

template <typename  T>
void printAnyStack(IStack<T>* stack)
{
	std::cout << stack->print_to_string() << std::endl;
}
