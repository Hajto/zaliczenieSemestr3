#pragma once
#include "IStack.h"

template <typename T> struct Link
{
	T value;
	Link<T>* next = nullptr;
};

template <typename T> 
class LinkedListStack : public IStack<T> {
public:
	LinkedListStack<T>() {}
	~LinkedListStack<T>();

	//IStack members override
	void push(T elem) override;
	T pop() override;
	T head() override;
	void reverse() override;
	int length() override;
	bool isEmpty() override;

	//IEnumerable members
	void foreach(EnumerableCallback) override;

private:
	int count = 0;
	Link<T>* next = nullptr;
};