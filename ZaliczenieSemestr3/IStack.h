#pragma once
#include "IEnumerable.h"
#include "IPrintable.h"
#include <iostream>  
#include <string>

template <class T>
class IStack : public IEnumerable<T>, IPrintable
{
public:
	virtual ~IStack() {}
	virtual void push(T elem) = 0;
	virtual T pop() = 0;
	virtual T head() = 0;
	virtual void reverse() = 0;
	virtual int length() = 0;
	virtual bool isEmpty() = 0;

	virtual void operator+(IStack<T>* stack)
	{
		stack->foreach([this](T elem) -> void {
			push(elem);
		});
	}
	std::string print_to_string() override {
		std::string temp = "";
		
		foreach([&temp, this](T elem) -> void {
			std::string result = IPrintable::print_to_string<T>(elem);
			temp += "\t";
			temp += result;
			temp += "\n";
		});

		return temp;
	}
};

template<typename T>
std::ostream& operator<< (std::ostream& stream, IStack<T>* stack) {
	stream << stack->print_to_string();
	return stream;
}

class StackAlreadyEmptyException : public std::exception
{
	std::string what_message = "Stack is already empty! Can't pop it more!";
public:
	virtual const char* what() const throw()
	{
		return what_message.c_str();
	}
};