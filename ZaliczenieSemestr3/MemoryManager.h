#pragma once

class MemoryManager {
public:
	MemoryManager();
	~MemoryManager();
};

struct MemoryAllocationNode {
	void* allocated;
	size_t when;
};

void* MemAllocate(size_t how_much);
void MemFree(void *ptr);

